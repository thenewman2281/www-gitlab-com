---
layout: handbook-page-toc
title: Getting started with Product Intelligence
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Choosing the right tool

There are two main tools that we use for tracking users data: [Service Ping](https://docs.gitlab.com/ee/development/service_ping/) and [Snowplow](https://docs.gitlab.com/ee/development/snowplow/index.html).
Here are the main differences between these two tools:

### Type of data
- Snowplow collects events which are interactions with the application, such as the date and time of visit, and the feature and functionality that has been clicked on or used.
- Service Ping reports only cumulative counts of things (ex. [count.epics](https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/config/metrics/counts_all/20210216181206_epics.yml) and [analytics_unique_visits.p_analytics_repo](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/counts_all/20210216174856_p_analytics_repo.yml) and settings/instance information (eg. [database.version](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/settings/20210216175609_version.yml) and [container_registry_enabled](https://gitlab.com/gitlab-org/gitlab/-/blob/master/config/metrics/settings/20210204124858_container_registry_enabled.yml)).

### Cadence
- Snowplow events are collected and sent to the data warehouse as they occur.
- Service Ping is collected from individual self-managed instances via an automated process weekly according to a [random distribution](https://gitlab.com/gitlab-org/gitlab/-/issues/352457#note_839039786) schedule, whereas SaaS Service Ping is collected weekly in a single payload via a [manual process](https://gitlab.com/groups/gitlab-org/-/epics/6000).

### Scope
- Snowplow event collection is currently only available in SaaS.
- Service Ping is available for both self-managed and SaaS.
- Snowplow can collect events for both frontend and backend activities, while Service Ping only offers access to backend data.

### Availability
- When new Service Ping metrics are instrumented, only self-managed instances which have upgraded to the GitLab version in which the metric instrumentation is available will start reporting applicable data.
- When new Snowplow events are instrumented, data will begin flowing to the data warehouse immediately once the code is deployed to GitLab.

### Analysis
- Snowplow events can be parsed downstream by `namespace_id`, `project_id` and/or [pseudonyminized](https://gitlab.com/gitlab-org/gitlab/-/issues/343965) `user_id`
- Google Analytics ID and Snowplow ID are mapped which allow downstream analytics of ([pseudonyminized](https://gitlab.com/gitlab-org/gitlab/-/issues/343965)) user journeys inside and outside the product. ([issue reference](https://gitlab.com/gitlab-org/gitlab/-/issues/335067))
- Self-managed Service Ping product usage data can be tied to customers, but SaaS Service Ping data cannot currently be tied to the customer/namespace level.
- Snowplow's data is enriched with browser-specific metadata: for example, name of the used browser, user timezone, page url.
- Snowplow automatically records all `page view` events on GitLab.

### Example use cases for Service Ping

- track how many different labels exist on given GitLab instance *(it cannot be tracked with Snowplow, because it is not an **event**. With Snowplow, we would be able to track - for example - how many times has the label creation event happened)*
- track whether a GitLab instance has the Gravatar feature enabled *(it cannot be tracked with Snowplow, because it is not an **event** - it's a metric specific for a given instance)*

### Example use cases for Snowplow

- track how many users entered the `Issue board` page *(Snowplow already records all page views - there's no need for adding an additional Service Ping metric for that)*
- track how many times have users clicked the "new issue" button *(clicking a button is a frontend-only event - it cannot be tracked with Service Ping)*
- track how many users entered the `Pricing` page after checking the `Partners` page *(Snowplow is able to track [user journey](https://about.gitlab.com/handbook/product/product-intelligence-guide/#example-user-journey))*
- track how many users are viewing the handbook in Firefox *(Snowplow events include browser metadata - in Service Ping, we don't have access to that data)*

## Event/metric instrumentation

### Service Ping

Service Ping consists of two kinds of data:

- **Counters**: Track how often a certain event happened over time, such as how many CI/CD pipelines have run. They are monotonic and always trend up.
- **Observations**: Facts collected from one or more GitLab instances and can carry arbitrary data. There are no general guidelines for how to collect those, due to the individual nature of that data. An example of this data would be an instance's database version.

The next step after deciding on the new metrics' data type is choosing the metric counter type.

#### 1. Metric counter types

The metric counter type depends on the type of data to be tracked:

- **Database metric** - used for tracking data kept in the database, for example - count of Issues existing on given instance.
- **Redis metric** - used for tracking events that are not kept in the database, for example - count of how many times the search bar has been used.
- **RedisHLL metric** - used for tracking events that are not kept in the database and should only be incremented for unique values (usually - unique users), for example - a count of how many different users used the search bar.
- **Generic metric** - used for other types of metrics, for example - an instance's database version. **Observations** type of data will always have a **Generic metric** counter type.

After determining the metric counter type, it's time to implement the counter incrementing!

#### 2. Adding the counter incrementation logic

This step can be skipped for **Database metrics** and **Generic metrics**. These metrics do not need separate counter incrementation logic because they grab the data straight from GitLab's database/config.

##### Redis counter implementation

For Redis counters, we offer a frontend API described [in the Service Ping guide](https://docs.gitlab.com/ee/development/service_ping/implement.html#usagedata-api), and a JavaScript/Vue helper for using this API.

##### RedisHLL counter implementation

To add a RedisHLL counter you must add the event definition and then implement the counter incrementation.

To define a new event, you must add a `yml` file to the [`known_events` folder](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/usage_data_counters/known_events). See [Add new events](https://docs.gitlab.com/ee/development/service_ping/implement.html#add-new-events) for detailed information about defining events.

For RedisHLL counters, you can implement the counter incrementation logic in multiple ways.

In the backend you can implement the counter incrementation logic in:

  - The controller, using the `RedisTracking` module.
  - The api, using the `increment_unique_values` method.
  - Other services, using the `track_usage_event` method.

In the frontend you can implement the counter incrementation logic in the `POST /usage_data/increment_unique_users` API endpoint and its JavaScript/Vue helper.

The tracking methods' implementation is described in detail in the point `2` of [the Service Ping guide](https://docs.gitlab.com/ee/development/service_ping/implement.html#redis-hll-counters).


#### 3. Instrumentation class

Now that the events are being recorded, the next step is adding an instrumentation class that will define how they are counted.
The instrumentation class implementation will depend on the metrics counter type.

##### Database metric

See [Database metrics](https://docs.gitlab.com/ee/development/service_ping/metrics_instrumentation.html#database-metrics) for a definition of the database metrics instrumentation class.

##### Redis metric

Redis metrics typically don't require adding an instrumentation class - instead, they reuse the already defined `RedisMetric` class. A new instrumentation class only needs to be added if we need to define [the metric's availability](https://docs.gitlab.com/ee/development/service_ping/metrics_instrumentation.html#availability-restrained-redis-metrics).

##### RedisHLL metric

RedisHLL metrics typically don't require adding an instrumentation class - instead, they reuse the already defined `RedisHLLMetric` class. A new instrumentation class only needs to be added if we need to define [the metric's availability](https://docs.gitlab.com/ee/development/service_ping/metrics_instrumentation.html#availability-restrained-redis-hyperloglog-metrics).

##### Generic metric

See [Generic metrics](https://docs.gitlab.com/ee/development/service_ping/metrics_instrumentation.html#generic-metrics) for a definition of the generic metrics instrumentation class.

#### 4. Adding the event to Service Ping payload & Metrics Dictionary

Now that the events have an instrumentation class defined, the next step is adding them to the Service Ping data payload and to the [Metrics Dictionary](https://metrics.gitlab.com/). Both of these goals can be achieved by adding a single YML event configuration file.

See [Metrics Definition and validation](https://docs.gitlab.com/ee/development/service_ping/metrics_dictionary.html#metrics-definition-and-validation) for the instructions to add the YML event configuration file .

The **instrumentation class**, defined in the previous step of this guide, should be used as the value for the `instrumentation_class` YAML attribute of the newly created config file.

### Snowplow

There are multiple ways of implementing Snowplow tracking, depending on the framework used. However, regardless of the framework used, the events need to have at least two main attributes defined: their `action` and `category`. The values that these (and other) attributes should take are explained with examples in the [event taxonomy guide](https://docs.gitlab.com/ee/development/snowplow/index.html#structured-event-taxonomy). It's also possible to see the structure of existing events in the [Metrics Dictionary](https://metrics.gitlab.com/snowplow).
The way in which those properties are passed to `Snowplow` depends on framework used.

The framework options with their respective guides are:
- [Data-track html attribute](https://docs.gitlab.com/ee/development/snowplow/implementation.html#implement-data-attribute-tracking) - this is the default tracking method for frontend events. If you need more customization than this method of tracking has to offer, you can implement the tracking using one of the following guides:
  - [Vue](https://docs.gitlab.com/ee/development/snowplow/implementation.html#implement-vue-component-tracking)
  - [Raw JavaScript](https://docs.gitlab.com/ee/development/snowplow/implementation.html#implement-raw-javascript-tracking)
- [Ruby](https://docs.gitlab.com/ee/development/snowplow/implementation.html#implement-ruby-backend-tracking) - used for tracking events on backend

## How to validate your code

### Snowplow

To verify the added changes using your local environment, check [local setup instructions](https://docs.gitlab.com/ee/development/service_ping/implement.html#set-up-and-test-service-ping-locally).

To make sure the MR is ready for review, check out the [review guidelines](https://docs.gitlab.com/ee/development/snowplow/review_guidelines.html#the-merge-request-author-should). They also include information about what the Product Intelligence reviewer will later check in the MR.

If you want to verify the event attributes passed to Snowplow, check out the [event taxonomy guide](https://docs.gitlab.com/ee/development/snowplow/index.html#structured-event-taxonomy). You can also check the [Metrics dictionary](https://metrics.gitlab.com/snowplow/) for examples of already existing implementations.

### Service Ping

To verify the added changes using your local environment, check [Snowplow testing guide](https://docs.gitlab.com/ee/development/snowplow/implementation.html#develop-and-test-snowplow).

To make sure the MR is ready for review, check out the [review guidelines](https://docs.gitlab.com/ee/development/service_ping/review_guidelines.html#the-merge-request-author-should). They also include information about what the Product Intelligence reviewer will later check in the MR.

## When to create a Data team issue

Create a [Data team issue](https://gitlab.com/gitlab-data/analytics/-/issues) when a database value you want to use is not available in Sisense ([handbook doumentation](https://about.gitlab.com/handbook/business-technology/data-team/programs/data-for-product-managers/#what-if-the-table-or-column-i-want-isnt-in-the-data-warehouse), [example use case](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17476#note_935980398), [issue template](https://gitlab.com/gitlab-data/analytics/-/issues/new?issuable_template=New%20Data%20Source)).

## When to create a Product Analysis team issue

You should create a [Product Analysis team issue](https://gitlab.com/gitlab-data/product-analytics/-/issues/new?issuable_template=Ad%20Hoc%20Request) when you encounter one of these situations:
- you want to introduce changes into [xMAU metrics](https://about.gitlab.com/handbook/business-technology/data-team/data-catalog/xmau-analysis/) ([example use case](https://gitlab.com/gitlab-data/analytics/-/issues/11662) - introducing a new UMAU metric)
- you need help creating or updating a Sisense dashboard
- you want to ensure that the metrics you are instrumenting will allow you to answer your questions or support analysis

## How to verify tracked data

After the new code has been pushed to production, it's time to verify the tracked data. Depending on the tool used, the data's [cadence](handbook/product/product-intelligence-guide/getting-started#cadence) and [availability](handbook/product/product-intelligence-guide/getting-started#availability) will vary.

### Service Ping data availability

New Service Ping data will only be collected from instances running the newly added code. This means that most likely, the first piece of data that you will be able to verify will come from the GitLab SaaS instance. For the first Service Ping containing deployed changes to appear in Sisense, you will need to wait this much time:
  - For the data from Gitlab SaaS instance, the data should appear in, at most, 10 days from the time the code changes have been deployed to production.
  - For the data from self-managed instances, the timing depends on the release cycle. Check more in-depth information about the data availability in the [Product Intelligence guide](https://about.gitlab.com/handbook/product/product-intelligence-guide/#3-data-availability).

### Snowplow data availability

New Snowplow data will start getting collected right after the new changes are deployed to production. However, since the data needs to get processed by database pipelines, you may need to wait [24 hours](https://about.gitlab.com/handbook/business-technology/data-team/platform/#extract-and-load) for it to make its way to Sisense.

### Accessing the data in Sisense

To access the data in Sisense, you will need to create a dashboard with a chart containing the new metric data.

We have this process (for the Service Ping case) illustrated in [a video](https://www.youtube.com/watch?v=n4o65ivta48).
This is what needs to be done:

1. Create a testing dashboard in Sisense ([example dashboard](https://app.periscopedata.com/app/gitlab/904191/Alina's-testing-dashboard)). In case you don't have permissions to create a Dashboard, you will need to create an Access Request like [this one](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/13238).
1. Add a new chart to the dashboard by clicking the `New chart` button.
1. Build the SQL query.
  - If you added a Service Ping event, you can go to the [Metrics dictionary](https://metrics.gitlab.com/), find your new metric and click the "copy query to clipboard" button.
  - If you added a Snowplow event, you can use one of the [example queries](https://docs.gitlab.com/ee/development/snowplow/#reference-sql) by modifying their `event_action`/`event_label` parameters.
1. Insert the query into the `SQL` field of the new chart and click "Run SQL".
1. Your data should now be visible in Sisense.


## Quick Links

| Resource | Description |
| -------- | ----------- |
| [Sisense handbook page](https://about.gitlab.com/handbook/business-technology/data-team/platform/periscope/#self-service-dashboard-development-2) | A guide for getting started with SiSense |
| [Metrics dictionary](https://metrics.gitlab.com/) | A SSoT for all collected metrics from Usage Ping |
| [dbt data tool](https://gitlab-data.gitlab.io/analytics/#!/overview) | A tool for viewing relations between databases |
| [FAQ](handbook/product/product-intelligence-guide/faq) | Product Intelligence FAQ |
